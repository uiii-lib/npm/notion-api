import objectHash from "object-hash";

import {
	APIErrorCode,
	APIResponseError,
	Client,
	isFullBlock,
	isFullDatabase,
	isFullPage,
	RequestTimeoutError,
} from "@notionhq/client";
import {
	BlockObjectResponse,
	CreatePageParameters,
	GetDatabaseParameters,
	GetPageParameters,
	GetPagePropertyParameters,
	ListBlockChildrenResponse,
	PageObjectResponse,
	QueryDatabaseParameters,
	UpdateDatabaseParameters,
	UpdatePageParameters,
} from "@notionhq/client/build/src/api-endpoints.js";
import { ensure, ensured, sleep, take } from "@uiii-lib/utils";

import { RateLimitError } from "./errors/rate-limit.error.js";
import { Cache } from "./model/cache.js";
import { GetChildrenOptions } from "./model/get-children-options.js";
import {
	NotionApiCacheableRequestOptions,
	NotionApiRequestOptions,
} from "./model/notion-api-request-options.js";
import { PaginatedArgs } from "./model/paginated-args.js";
import { PaginatedResponse } from "./model/paginated-response.js";
import { SearchOptions } from "./model/search-options.js";

export interface AuthData {
	accessToken: string;
}

type BlockQueueNode =
	| { type: "parent"; parentId: string; nextCursor?: string }
	| {
			type: "block";
			block: ListBlockChildrenResponse["results"][number];
	  };

const retryableErrorCodes: APIErrorCode[] = [
	APIErrorCode.ConflictError,
	APIErrorCode.InternalServerError,
	APIErrorCode.ServiceUnavailable,
];

export interface NotionApiOptions {
	debug?: boolean;
}

export class NotionApi {
	readonly client: Client;

	protected cacheableFns: WeakMap<any, string>;

	constructor(
		protected cache?: Cache,
		protected sentry?: any,
		protected options: NotionApiOptions = {},
	) {
		this.client = new Client({
			fetch: this.fetchApi.bind(this),
		});

		this.cacheableFns = new WeakMap<any, string>([
			[this.client.blocks.children.list, "blocks.children.list"],
			[this.client.blocks.retrieve, "blocks.retrieve"],
			[this.client.blocks.retrieve, "blocks.retrieve"],
			[this.client.databases.retrieve, "databases.retrieve"],
			[this.client.databases.query, "databases.query"],
			[this.client.pages.retrieve, "pages.retrieve"],
			[this.client.pages.properties.retrieve, "pages.properties.retrieve"],
			[this.client.search, "search"],
		]);
	}

	async searchPages(auth: AuthData, query: string, options: SearchOptions) {
		options = options || {
			count: 5,
			fillUp: false,
		};

		const pages = await this.doSearchPages(
			auth,
			query.replace(/\s+/g, ""),
			undefined,
			options,
		);

		if (pages.length < options.count) {
			pages.push(
				...(await this.doSearchPages(
					auth,
					query,
					(it) => !pages.find((p) => p.id === it.id),
					{ ...options, count: options.count - pages.length },
				)),
			);
		}

		if (pages.length < options.count && options.fillUp) {
			pages.push(
				...(await this.doSearchPages(
					auth,
					"",
					(it) => !pages.find((p) => p.id === it.id),
					{ ...options, count: options.count - pages.length },
				)),
			);
		}

		return pages;
	}

	protected async doSearchPages(
		auth: AuthData,
		query: string,
		filter: ((page: PageObjectResponse) => boolean) | undefined,
		options: SearchOptions,
	) {
		return take(
			options.count,
			this.iterate(
				this.client.search,
				{
					auth: auth.accessToken,
					query,
					filter: {
						property: "object",
						value: "page",
					},
					sort: {
						timestamp: "last_edited_time",
						direction: "descending",
					},
					page_size: Math.min(options.count, 100),
				},
				(it): it is PageObjectResponse =>
					isFullPage(it) && (!filter || filter(it)),
				options,
			),
		);
	}

	async findBlock(
		auth: AuthData,
		parentId: string,
		predicate: (block: BlockObjectResponse) => boolean,
		options?: GetChildrenOptions,
	) {
		for await (const block of this.getChildren(auth, parentId, options)) {
			if (isFullBlock(block) && predicate(block)) {
				return block;
			}
		}
	}

	async findPageId(
		auth: AuthData,
		parentId: string,
		title: string,
		options?: GetChildrenOptions,
	) {
		const pageBlock = await this.findBlock(
			auth,
			parentId,
			(it) => it.type === "child_page" && it.child_page.title === title,
			options,
		);

		return pageBlock?.id;
	}

	async findDatabaseId(
		auth: AuthData,
		parentId: string,
		title: string,
		options?: GetChildrenOptions,
	) {
		const dbBlock = await this.findBlock(
			auth,
			parentId,
			(it) => it.type === "child_database" && it.child_database.title === title,
			options,
		);

		return dbBlock?.id;
	}

	async findDatabase(
		auth: AuthData,
		parentId: string,
		title: string,
		options?: GetChildrenOptions,
	) {
		const databaseId = await this.findDatabaseId(
			auth,
			parentId,
			title,
			options,
		);

		if (!databaseId) {
			return undefined;
		}

		return ensure(
			await this.request(
				this.client.databases.retrieve,
				{
					auth: auth.accessToken,
					database_id: databaseId,
				},
				options,
			),
			isFullDatabase,
		);
	}

	async *queryDatabase(
		auth: AuthData,
		params: QueryDatabaseParameters,
		options?: NotionApiCacheableRequestOptions,
	) {
		yield* this.iterate(
			this.client.databases.query,
			{
				auth: auth.accessToken,
				...params,
			},
			ensured(isFullPage),
			options,
		);
	}

	async queryDatabaseCount(
		auth: AuthData,
		params: QueryDatabaseParameters,
		options?: NotionApiCacheableRequestOptions,
	) {
		return this.count(
			this.client.databases.query,
			{
				auth: auth.accessToken,
				...params,
				filter_properties: ["title"],
			},
			ensured(isFullPage),
			options,
		);
	}

	async *getChildren(
		auth: AuthData,
		parentId: string,
		options?: GetChildrenOptions,
	) {
		// set default options
		options = {
			nest: "after",
			depth: undefined,
			...options,
		};

		const pageQueue = [parentId];

		let currentDepth = 0;

		while (pageQueue.length > 0) {
			const pageId = pageQueue.shift()!;

			if (options.depth !== undefined && currentDepth > options.depth) {
				return;
			}

			const blockQueue: BlockQueueNode[] = [
				{ type: "parent", parentId: pageId },
			];

			let afterChildBlocksQueue: BlockQueueNode[] = [];

			while (blockQueue.length > 0) {
				const node = blockQueue.shift()!;

				if (node.type === "block") {
					yield ensure(node.block, isFullBlock);
				} else if (node.type === "parent") {
					const children = await this.request(
						this.client.blocks.children.list,
						{
							auth: auth.accessToken,
							block_id: node.parentId,
							start_cursor: node.nextCursor,
						},
						options,
					);

					const childBlocksQueue: BlockQueueNode[] = [];

					for (const child of children.results) {
						childBlocksQueue.push({
							type: "block",
							block: child,
						});

						if (isFullBlock(child) && child.has_children) {
							if (child.type === "child_page") {
								pageQueue.push(child.id);
							} else if (options.nest === "immediately") {
								childBlocksQueue.push({
									type: "parent",
									parentId: child.id,
								});
							} else if (options.nest === "after") {
								afterChildBlocksQueue.push({
									type: "parent",
									parentId: child.id,
								});
							}
						}
					}

					if (children.has_more && children.next_cursor) {
						childBlocksQueue.push({
							type: "parent",
							parentId: node.parentId,
							nextCursor: children.next_cursor,
						});
					} else {
						childBlocksQueue.push(...afterChildBlocksQueue);
						afterChildBlocksQueue = [];
					}

					blockQueue.unshift(...childBlocksQueue);
				}
			}

			++currentDepth;
		}
	}

	async getPage(
		auth: AuthData,
		data: GetPageParameters,
		options?: NotionApiCacheableRequestOptions,
	) {
		return ensure(
			await this.request(
				this.client.pages.retrieve,
				{
					auth: auth.accessToken,
					...data,
				},
				options,
			),
			isFullPage,
		);
	}

	async getDatabase(
		auth: AuthData,
		data: GetDatabaseParameters,
		options?: NotionApiCacheableRequestOptions,
	) {
		return ensure(
			await this.request(
				this.client.databases.retrieve,
				{
					auth: auth.accessToken,
					...data,
				},
				options,
			),
			isFullDatabase,
		);
	}

	async getPageProperty(
		auth: AuthData,
		data: GetPagePropertyParameters,
		options?: NotionApiCacheableRequestOptions,
	) {
		return this.request(
			this.client.pages.properties.retrieve,
			{
				auth: auth.accessToken,
				...data,
			},
			options,
		);
	}

	async createPage(
		auth: AuthData,
		data: CreatePageParameters,
		options?: NotionApiRequestOptions,
	) {
		return this.request(
			this.client.pages.create,
			{
				auth: auth.accessToken,
				...data,
			},
			options,
		);
	}

	async updatePage(
		auth: AuthData,
		data: UpdatePageParameters,
		options?: NotionApiRequestOptions,
	) {
		return this.request(
			this.client.pages.update,
			{
				auth: auth.accessToken,
				...data,
			},
			options,
		);
	}

	async updateDatabase(
		auth: AuthData,
		data: UpdateDatabaseParameters,
		options?: NotionApiRequestOptions,
	) {
		return this.request(
			this.client.databases.update,
			{
				auth: auth.accessToken,
				...data,
			},
			options,
		);
	}

	protected async *iterate<
		Args extends PaginatedArgs,
		Item,
		Filtered extends Item = Item,
	>(
		fn: (args: Args) => Promise<PaginatedResponse<Item>>,
		args: Args,
		filter?: (item: Item) => item is Filtered,
		options?: NotionApiCacheableRequestOptions,
	): AsyncIterableIterator<Filtered> {
		let nextCursor: string | null | undefined = args.start_cursor;

		do {
			const response: PaginatedResponse<Item> = await this.request(
				fn,
				{
					...args,
					start_cursor: nextCursor,
				},
				options,
			);

			yield* response.results.filter(filter || (() => true)) as any;

			nextCursor = response.next_cursor;
		} while (nextCursor);
	}

	protected async count<
		Args extends PaginatedArgs,
		Item,
		Filtered extends Item = Item,
	>(
		fn: (args: Args) => Promise<PaginatedResponse<Item>>,
		args: Args,
		filter?: (item: Item) => item is Filtered,
		options?: NotionApiCacheableRequestOptions,
	) {
		let nextCursor: string | null | undefined = args.start_cursor;

		let total = 0;

		do {
			const response: PaginatedResponse<Item> = await this.request(
				fn,
				{
					...args,
					start_cursor: nextCursor,
				},
				options,
			);

			total += response.results.filter(filter || (() => true)).length;

			nextCursor = response.next_cursor;
		} while (nextCursor);

		return total;
	}

	async request<Args, Response>(
		fn: (args: Args) => Promise<Response>,
		args: Args,
		options?: NotionApiCacheableRequestOptions,
	): Promise<Response> {
		const retryCount = options?.retryCount || 0;

		performance.mark("request-start");
		try {
			const response = await this.doRequest(fn, args, options);
			return response;
		} catch (e) {
			if (e instanceof RateLimitError) {
				this.sentry?.captureMessage("Request rate limited", {
					extra: {
						retryAfterMs: e.retryAfterMs,
					},
				});
				console.warn(
					`WARNING: Request rate limited! Retry after ${e.retryAfterMs}ms`,
				);
				await sleep(e.retryAfterMs);
				return await this.doRequest(fn, args, options);
			}

			if (
				e instanceof RequestTimeoutError ||
				(e instanceof APIResponseError && retryableErrorCodes.includes(e.code))
			) {
				this.sentry?.captureMessage(
					`Request failed with ${e.code} (retry count ${retryCount})`,
					(scope: any) => {
						scope.setFingerprint([
							"notion retryable error",
							e.code,
							retryCount.toString(),
						]);
						return scope;
					},
				);

				const retryAfter = Math.pow(3, retryCount + 1) * 1000;

				console.warn(
					`WARNING: Request failed with ${e.code}${retryCount > 0 ? ` (Retry count ${retryCount})` : ""}! ` +
						(retryCount < 3
							? `Retry after ${retryAfter}ms.`
							: `Retry limit reached.`),
				);

				if (retryCount < 3) {
					await sleep(retryAfter);

					return await this.request(fn, args, {
						...options,
						retryCount: retryCount + 1,
					});
				}
			}

			throw e;
		} finally {
			performance.mark("request-end");

			if (this.options.debug) {
				console.debug(
					"Request",
					Math.round(
						performance.measure("request-time", "request-start", "request-end")
							.duration,
					)
						.toString()
						.padStart(5, " "),
					fn,
					JSON.stringify({ ...args, auth: undefined }).slice(0, 50),
				);
			}
		}
	}

	protected async doRequest<Args, Response>(
		fn: (args: Args) => Promise<Response>,
		args: Args,
		options?: NotionApiCacheableRequestOptions,
	) {
		if (this.cache && options?.cache && this.cacheableFns.has(fn)) {
			const cacheOptions =
				typeof options.cache === "object" ? options.cache : {};

			const cacheKey = objectHash([this.cacheableFns.get(fn), args]);

			return await this.cache.get(cacheKey, () => fn(args), cacheOptions);
		}

		return await fn(args);
	}

	protected async fetchApi(url: string, init?: RequestInit): Promise<Response> {
		const response = await fetch(url, init);

		if (response.status === 429) {
			const retryAfter = parseInt(response.headers.get("Retry-After") || "10");

			if (this.options.debug) {
				console.debug("RATE LIMITED! Retry after", retryAfter, "seconds!");
			}

			throw new RateLimitError(retryAfter * 1000);
		}

		return response;
	}
}
