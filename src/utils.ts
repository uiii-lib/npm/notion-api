import {
	DatabaseObjectResponse,
	PageObjectResponse,
} from "@notionhq/client/build/src/api-endpoints.js";

type FindByType<Union, Type> = Union extends { type: Type } ? Union : never;

export function getProperty<
	O extends PageObjectResponse | DatabaseObjectResponse,
	T extends O["properties"][string]["type"],
>(item: O, type: T, name?: string): FindByType<O["properties"][string], T> {
	const prop = Object.entries(item.properties).find(
		(it) => it[1].type === type && (!name || it[0] === name),
	)?.[1] as FindByType<PageObjectResponse["properties"][string], T>;

	if (!prop) {
		throw new Error(`Cannot get property '${name}'`);
	}

	return prop as FindByType<O["properties"][string], T>;
}

export function getTitle<O extends PageObjectResponse | DatabaseObjectResponse>(
	item: O,
) {
	return getProperty(item, "title")
		.title.map((it) => it.plain_text)
		.join("");
}
