export interface CacheOptions {
	context?: string[];
	ttl?: number;
}

export interface Cache {
	get<T>(
		key: string,
		init?: () => Promise<T>,
		options?: CacheOptions,
	): Promise<T>;
	set<T>(key: string, value: T, options?: CacheOptions): void;
	delete(key: string, options?: Omit<CacheOptions, "ttl">): void;
}
