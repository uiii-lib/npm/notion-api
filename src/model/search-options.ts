import { NotionApiCacheableRequestOptions } from "./notion-api-request-options.js";

export interface SearchOptions extends NotionApiCacheableRequestOptions {
	count: number;
	fillUp?: boolean;
}
