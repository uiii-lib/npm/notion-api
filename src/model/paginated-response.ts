export interface PaginatedResponse<T> {
	object: "list";
	results: T[];
	next_cursor: string | null;
	has_more: boolean;
}
