export interface NotionApiRequestOptions {
	retryCount?: number;
}

export interface NotionApiCacheableRequestOptions
	extends NotionApiRequestOptions {
	cache?:
		| boolean
		| {
				context?: string[];
				ttl?: number;
		  };
}
