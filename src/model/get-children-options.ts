import { NotionApiCacheableRequestOptions } from "./notion-api-request-options.js";

export interface GetChildrenOptions extends NotionApiCacheableRequestOptions {
	nest?: "immediately" | "after" | false;
	depth?: number;
}
