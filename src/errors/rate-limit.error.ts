import { CustomError } from "ts-custom-error";

export class RateLimitError extends CustomError {
	constructor(public readonly retryAfterMs: number) {
		super(`Request rate limited! Retry after ${retryAfterMs}`);
	}
}
