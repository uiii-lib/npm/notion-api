import eslintConfig from "@uiii-lib/eslint-config";

export default [
	...eslintConfig,
	{
		languageOptions: {
			parserOptions: {
				tsconfigDirName: import.meta.dirname,
			},
		},
	},
];
